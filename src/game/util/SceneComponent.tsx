import { Nullable, EngineOptions } from '@babylonjs/core';
import React, { useEffect, useRef, useState } from 'react';
import { GameScene } from '../interface';
import GameInstanceManager from '../manager/GameInstanceManager';

export type BabylonjsProps = {
    antialias?: boolean;
    engineOptions?: EngineOptions;
    adaptToDeviceRatio?: boolean;
    scene: GameScene;
    id: string;
};

export default (props: BabylonjsProps) => {
    const reactCanvas = useRef<Nullable<HTMLCanvasElement>>(null);
    const { antialias, engineOptions, adaptToDeviceRatio, scene, ...rest } = props;

    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        if (!loaded) {
            setLoaded(true);

            new GameInstanceManager({
                canvas: reactCanvas.current,
                antialias,
                engineOptions,
                adaptToDeviceRatio
            });

            const { sceneOptions, onSceneReady, onRender } = scene;

            GameInstanceManager.Instance?.startScene({
                sceneOptions,
                onSceneReady,
                onRender
            });
        }
    }, [adaptToDeviceRatio, antialias, engineOptions, loaded, reactCanvas, scene]);

    return (
        <canvas ref={reactCanvas} {...rest} />
    );
}