import { Scene, Camera } from '@babylonjs/core';

/**
 * Adjusts camera field of view to ensure that object size remains relatively consistent to window size
 * @param scene Current Babylon scene
 * @param camera Current Babylon camera
 */
export function adjustCamera(scene: Scene, camera: Camera) {
    if (scene.getEngine().getRenderHeight() > scene.getEngine().getRenderWidth()) {
        camera.fovMode = Camera.FOVMODE_VERTICAL_FIXED;
    }
    else {
        camera.fovMode = Camera.FOVMODE_VERTICAL_FIXED;
    }
}

/**
 * Registers an observer that automatically adjusts camera field of view on window resize
 * @param scene Current Babylon scene
 * @param camera Current Babylon camera
 */
export function setupCameraAutoAdjust(scene: Scene, camera: Camera) {
    const observer = scene.getEngine().onResizeObservable.add(() => {
        adjustCamera(scene, camera);
    });
    adjustCamera(scene, camera);
    return observer;
}

