import React from 'react';
import SceneComponent from './util/SceneComponent';
import FirstScene from './scene/FirstScene';

const startingScene = new FirstScene();

export default () => (
    <SceneComponent 
      antialias 
      adaptToDeviceRatio
      scene={startingScene} // First scene to render
      id='App-game' 
    />
)