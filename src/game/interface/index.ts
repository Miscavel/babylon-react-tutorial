import { Scene, SceneOptions } from '@babylonjs/core';

/**
 * Interface for scene rendered in game
 */
export interface GameScene {
    /**
     * Function that contains initialization of all objects used in that scene
     */
    onSceneReady: (scene: Scene) => void;
    /**
     * Function that contains what happens on each update of that scene
     */
    onRender?: (scene: Scene) => void;
    /**
     * Optional Babylon scene config
     */
    sceneOptions?: SceneOptions;
}