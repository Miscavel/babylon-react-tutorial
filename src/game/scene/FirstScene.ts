import { 
    FreeCamera, 
    Vector3, 
    HemisphericLight, 
    MeshBuilder, 
    Mesh, 
    Scene, 
    KeyboardEventTypes,
    ArcRotateCamera,
    PointLight,
    StandardMaterial,
    Color3,
    PhysicsImpostor,
    CannonJSPlugin,
    Vector2,
    Material,
    Animation,
    PointerEventTypes
} from '@babylonjs/core';
import {
    AdvancedDynamicTexture,
    TextBlock
} from '@babylonjs/gui';
import { setupCameraAutoAdjust } from '../util/CameraAdjustment';
import { GameScene } from '../interface';
import cannon from 'cannon';
import GameInstanceManager from '../manager/GameInstanceManager';

export default class FirstScene implements GameScene {
    properties = {
        base: {
            diameterTop: 3, 
            diameterBottom: 3, 
            arc: 1, 
            tessellation: 24, 
            height: 0.25, 
            sideOrientation: Mesh.DOUBLESIDE, 
            enclose: true
        },
        pole: {
            diameterTop: 1, 
            diameterBottom: 1, 
            arc: 1, 
            tessellation: 24, 
            height: 20, 
            sideOrientation: Mesh.DOUBLESIDE, 
            enclose: true
        }
    };

    platformCount = 10;

    gap = (this.properties.pole.height) / (this.platformCount + 1);

    ballPositioningRadius = (this.properties.base.diameterTop + this.properties.pole.diameterTop) / 4;

    currentBallAngle = 90 / 180 * Math.PI;

    previousAlpha?: number;

    camera?: ArcRotateCamera;

    gameOver = false;

    /**
     * Scene create function
     * @param scene Scene object that has been registered to Babylon engine
     */
    public onSceneReady = (scene: Scene) => {
        scene.enablePhysics(new Vector3(0, -9.81, 0), new CannonJSPlugin(true, 60, cannon));

        const canvas = scene.getEngine().getRenderingCanvas() as HTMLElement;

        var camera = new ArcRotateCamera(
            "Camera",
            0,
            0,
            0,
            new Vector3(0, 15, 0),
            scene
        );
        camera.attachControl(canvas, true);
        camera.setPosition(new Vector3(0, 20, 10));
        camera.target.y = this.platformCount * this.gap;

        // camera.lowerRadiusLimit = camera.upperRadiusLimit = camera.radius;
        camera.lowerBetaLimit = camera.upperBetaLimit = camera.beta;
        this.camera = camera;
        
        var light1 = new HemisphericLight(
            "light1",
            new Vector3(1, 1, 0),
            scene
        );

        const ball = MeshBuilder.CreateSphere("ball", {diameter: 0.3}, scene);
        ball.position.z = this.ballPositioningRadius;
        ball.position.y = 20;
        ball.physicsImpostor = new PhysicsImpostor(ball, PhysicsImpostor.SphereImpostor, {mass: 1});
        ball.physicsImpostor.friction = 0;
        ball.physicsImpostor.executeNativeFunction(function (world, body) {
            body.fixedRotation = true;
            body.updateMassProperties();
        });

        const blueMat = new StandardMaterial("blueMat", scene);
        blueMat.diffuseColor = new Color3(0, 0, 1);

        ball.material = blueMat;

        this.previousAlpha = camera.alpha;
        scene.registerBeforeRender(() => {
            const diff =  (this.previousAlpha as number) - camera.alpha;
            this.previousAlpha = camera.alpha;

            ball.position.x = Math.cos(this.currentBallAngle);
            ball.position.z = Math.sin(this.currentBallAngle);

            this.currentBallAngle -= diff;

            if (ball.position.y < camera.target.y) {
                camera.target.y = ball.position.y;
            }
        });

        this.generatePlatform(
            scene, 
            ball,
            { min: 1, max: 3 }
        );

        /**
         * Sets auto-adjust on camera to ensure that all objects have the same size with 
         * respect to screen size
         */
        setupCameraAutoAdjust(scene, camera);
        // this.setupControl(scene);
        // this.setupGUI(scene);
    }

    generatePlatform(
        scene: Scene, 
        ball: Mesh,
        holeCount = { min: 1, max: 1 },
        holeArcLimit = { min: 0.075, max: 0.2 },
        platformArcLimit = { min: 0.075 },
        dangerChance = 0.1,
        dangerCountLimit = 3,
        dangerArcLimit = { min: 0.05, max: 0.2 }
    ) {
        const base = MeshBuilder.CreateCylinder("base", this.properties.base, scene);
        const pole = MeshBuilder.CreateCylinder("pole", this.properties.pole, scene);
        pole.position.y = base.position.y + (this.properties.base.height + this.properties.pole.height) / 2;
        
        base.physicsImpostor = new PhysicsImpostor(base, PhysicsImpostor.CylinderImpostor, {mass: 0});

        ball.physicsImpostor?.registerOnPhysicsCollide(base.physicsImpostor, () => {
            if (!this.gameOver) {
                ball.physicsImpostor?.dispose();
                this.camera?.detachControl(scene.getEngine().getRenderingCanvas() as HTMLElement);
                this.setupGUI(scene, "Victory! Tap to restart");
                this.gameOver = true;
            }
        });

        const platformPosition = {
            x: base.position.x,
            y: base.position.y + this.gap
        };

        const redMat = new StandardMaterial("redMat", scene);
        redMat.diffuseColor = new Color3(1, 0, 0);

        const greenMat = new StandardMaterial("greenMat", scene);
        greenMat.diffuseColor = new Color3(0, 1, 0);

        const blueMat = new StandardMaterial("blueMat", scene);
        blueMat.diffuseColor = new Color3(0, 0, 1);

        for (let i = 0; i < this.platformCount; i++) {
            const platformGroup: Array<Mesh> = [];
            let remainingArc = 1;

            /**
             * Generate hole arcs
             */
            const holeArcs = [];
            const holeCountLimit = Math.round(this.RNGBetween(holeCount.min, holeCount.max, 3));
            for (let i = 0; i < holeCountLimit; i++) {
                const arc = this.RNGBetween(holeArcLimit.min, holeArcLimit.max, 2);
                holeArcs.push(arc);
                
                remainingArc -= arc;
            }

            /**
             * Generate platform arcs
             */
            const platformArcs = [];
            for (let i = 0; i < holeCountLimit; i++) {
                const max = remainingArc / (holeCountLimit - i);
                
                const arc = this.RNGBetween(platformArcLimit.min, max, 2);
                platformArcs.push(arc);

                remainingArc -= arc;
            }
            if (remainingArc > 0) {
                platformArcs[platformArcs.length - 1] += remainingArc;
            } else if (remainingArc < 0) {
                console.log(remainingArc);
            }

            let totalArc = 0;
            let dangerCount = 0;
            while (holeArcs.length > 0 || platformArcs.length > 0) {
                const holeArc = holeArcs.pop();
                let platformArc = platformArcs.pop();

                if (holeArc) {
                    const hole = MeshBuilder.CreateCylinder(
                        "hole", 
                        {...this.properties.base, arc: holeArc, height: this.properties.base.height / 2}, 
                        scene
                    );
                    hole.position.y = platformPosition.y - this.properties.base.height / 2;
                    hole.material = blueMat;

                    hole.metadata = { type: "hole", active: true, arc: - totalArc - holeArc / 2 };

                    hole.rotate(new Vector3(0, 1, 0), totalArc * Math.PI * 2);

                    platformGroup.push(hole);

                    totalArc += holeArc;

                    hole.visibility = 0;
                }
                
                if (platformArc) {
                    while (platformArc > 0) {
                        let partitionArc = Math.min(
                            this.RNGBetween(dangerArcLimit.min, dangerArcLimit.max, 2),
                            platformArc
                        );

                        if (platformArc - partitionArc < dangerArcLimit.min) partitionArc = platformArc;

                        const platform = MeshBuilder.CreateCylinder(
                            "platform", 
                            {...this.properties.base, arc: partitionArc}, 
                            scene
                        );
                        platform.position.y = platformPosition.y;
                        
                        if (!platform.metadata) {
                            platform.metadata = { active: true, arc: - totalArc - partitionArc / 2 };
                        }

                        if (Math.random() < dangerChance && dangerCount < dangerCountLimit) {
                            platform.material = redMat;
                            platform.metadata.type = "danger";
                            dangerCount++;
                        } else {
                            platform.material = greenMat;
                            platform.metadata.type = "safe";
                        }

                        platform.rotate(new Vector3(0, 1, 0), totalArc * Math.PI * 2);
                        platformArc -= partitionArc;
                        totalArc += partitionArc;

                        platformGroup.push(platform);
                    }
                }
            }
            
            const randomRotation = Math.random() * Math.PI * 2;
            console.log('Begin');
            platformGroup.forEach((mesh) => {
                mesh.rotate(new Vector3(0, 1, 0), randomRotation);
                mesh.physicsImpostor = new PhysicsImpostor(mesh, PhysicsImpostor.MeshImpostor, {mass: 0});
                if (mesh.metadata.type == "hole") {
                    mesh.physicsImpostor.physicsBody.collisionResponse = false;
                }

                if (mesh.metadata.arc) {
                    const rotateAngle = mesh.metadata.arc * Math.PI * 2 - randomRotation;

                    mesh.metadata.finalPosition = {
                        x: mesh.position.x + Math.cos(rotateAngle) * this.properties.base.diameterTop * 0.25,
                        z: mesh.position.z + Math.sin(rotateAngle) * this.properties.base.diameterTop * 0.25
                    };

                    const lerpBox = new Animation("disappearAnimLerp", "position", 30,  Animation.ANIMATIONTYPE_VECTOR3);
                    let keys = [];
                    keys.push({
                        frame: 0,
                        value: new Vector3(
                            mesh.position.x,
                            mesh.position.y,
                            mesh.position.z
                        )
                    });
                    keys.push({
                        frame: 15,
                        value: new Vector3(
                            mesh.metadata.finalPosition.x,
                            mesh.position.y + this.properties.base.diameterTop * 0.25,
                            mesh.metadata.finalPosition.z
                        )
                    });
                    lerpBox.setKeys(keys);

                    const fadeBox = new Animation("disappearAnimFade", "visibility", 30,  Animation.ANIMATIONTYPE_FLOAT);
                    keys = [];
                    keys.push({
                        frame: 0,
                        value: mesh.visibility
                    });
                    keys.push({
                        frame: 20,
                        value: 0
                    });
                    fadeBox.setKeys(keys);

                    mesh.animations = [];
                    mesh.animations.push(lerpBox);
                    mesh.animations.push(fadeBox);
                }
                

                ball.physicsImpostor?.registerOnPhysicsCollide(mesh.physicsImpostor, () => {
                    if (!mesh.metadata.active) return;

                    const ballVelocY = ball.physicsImpostor?.getLinearVelocity()?.y as number;
                    switch(mesh.metadata.type) {
                        case "safe": {
                            if (ballVelocY <= 1) {
                                ball.physicsImpostor?.executeNativeFunction((world, body) => {
                                    body.velocity = new cannon.Vec3(0, 4, 0);
                                });
                            }
                            break;
                        }

                        case "danger": {
                            if (!this.gameOver) {
                                ball.physicsImpostor?.dispose();
                                this.camera?.detachControl(scene.getEngine().getRenderingCanvas() as HTMLElement);
                                this.setupGUI(scene, "Game Over! Tap to restart");
                                this.gameOver = true;
                            }
                            break;
                        }

                        case "hole": {
                            platformGroup.forEach((innerMesh) => {
                                innerMesh.metadata.active = false;

                                scene.beginAnimation(innerMesh, 0, 20, false);

                                // console.log(innerMesh.metadata.finalPosition);
                                // innerMesh.position.x = innerMesh.metadata.finalPosition.x;
                                // innerMesh.position.z = innerMesh.metadata.finalPosition.z;
                            });
                            break;
                        }
                    }
                });
            });

            platformPosition.y += this.gap;
        }
    }

    /**
     * Generates number between min (inclusive) and max (inclusive)
     * @param min Smallest random number possible
     * @param max Largest random number possible
     * @param randomPower Power applied to random number for weighting
     */
    RNGBetween(min: number, max: number, randomPower = 1) {
        return Math.min(min + Math.pow(Math.random(), randomPower) * (max - min), max);
    }

    setupControl(scene: Scene) {
        scene.onKeyboardObservable.add((kbInfo) => {
            switch (kbInfo.type) {
                case KeyboardEventTypes.KEYDOWN:
                    console.log("KEY DOWN: ", kbInfo.event.key);
                    break;
                case KeyboardEventTypes.KEYUP:
                    const keyCode = kbInfo.event.keyCode;
                    console.log("KEY UP: ", keyCode);
                    switch (keyCode) {
                        case 82:
                            GameInstanceManager.Instance?.startScene(new FirstScene());
                            break;
                    }
                    break;
            }
        });
    }

    setupGUI(scene: Scene, text: string) {
        var advancedTexture = AdvancedDynamicTexture.CreateFullscreenUI("UI", true, scene);
        /**
         * Ideal width is used for texture size reference.
         * E.g. if set to 720, then when the screen's width is only 360 all textures size would be halved
         */
        advancedTexture.idealWidth = 720;
        advancedTexture.renderAtIdealSize = true;

        var textObj = new TextBlock();
        textObj.text = text;
        textObj.color = "white";
        textObj.fontSize = 24;
        advancedTexture.addControl(textObj);    

        scene.onPointerObservable.add((pointerInfo) => {
            switch (pointerInfo.type) {
                case PointerEventTypes.POINTERDOWN:
                    GameInstanceManager.Instance?.startScene(new FirstScene());
                    break;
            }
        });
    }

    /**
     * Scene's update function
     */
    public onRender = (scene: Scene) => {
        // if (this.sphere) {
        //     var deltaTimeInMillis = scene.getEngine().getDeltaTime();

        //     const rpm = 10;
        //     this.sphere.rotation.y += ((rpm / 60) * Math.PI * 2 * (deltaTimeInMillis / 1000));
        // }
    }
}